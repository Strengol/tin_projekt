var express    = require('express');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');
var app        = express();
var http       = require('http').Server(app);
var async      = require('async');
var io         = require('socket.io')(http);
var _          = require('Underscore');
var less       = require('less-middleware');

mongoose.connect('mongodb://localhost/tin_projekt', function(error) {
  if(error) {
    console.log('Błąd połączenia z baza danych:' + error);
  } else {
    console.log('Polaczono z baza danych.');
  }
});

app.use(less(__dirname + '/public'));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use('/lib',express.static('./bower_components/jquery/dist'));


var czatujacy = [];
var sedzia = [];
io.sockets.on('connection',function(socket){

  socket.on('czatujacy',function(wprowadzony_nick,callback){
    if(czatujacy.indexOf(wprowadzony_nick) != -1){
      callback(false);
    } else {
      callback(true);
      socket.czatujacy = wprowadzony_nick;
      czatujacy.push(socket.czatujacy);
    }
  });

  socket.on('chat_data',function(data){
    io.sockets.emit('nick',data);
  });

  socket.on('check_password',function(dane,callback){
    if(dane != 'aaa'){
      callback(false);
    } else {
      callback(true);
      socket.sedzia = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      sedzia.push(socket.sedzia);
      io.sockets.emit('sedziowie',sedzia);
    }
  });

  socket.on('disconnect', function () {

    var pom = sedzia.indexOf(socket);
    sedzia.splice(pom,1);
    io.sockets.emit('sedziowie',sedzia);
  });

  socket.on('error',function(err){
    console.log(err);
  });

});

http.listen(8888,function(){
  console.log('listening on :8888');
});

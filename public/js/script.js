$(function(){
var socket;
var sedziowie           = $('#sedziowie');
var tajne_haslo_input   = $('#tajne_haslo_input');
var tajne_haslo_submit  = $('#tajne_haslo_submit');
var wprowadz_haslo      = $("#wprowadz_haslo");
var poczekalnia_sedziow = $("#poczekalnia_sedziow");

tajne_haslo_submit.click(function(){

  if(!socket || !socket.connected){
    socket = io({forceNew:true});
  }

    socket.emit('check_password',tajne_haslo_input.val(),function(data){
      if(data){
        wprowadz_haslo.remove();
        poczekalnia_sedziow.show();

      } else {
        tajne_haslo_input.css({"border":"solid 1px red"});
      }
    });

    socket.on('sedziowie',function(sedzia){
      var pom = '';
      for(i = 0; i < sedzia.length; i++){
        pom += '<li>'+ sedzia[i] +'</li>' ;
      }
      sedziowie.html(pom);
    });

  });

});
